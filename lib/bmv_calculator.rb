#Gerardo Gonzalez Cruz gerardogc2378@gmail.com

class CalculatorBMV
  #Tasa Forward
  def t_forward(tl, pl, tc, pc)
    "Tasa Forward: #{((1+(tl.to_f*pl.to_f/36000))/(1+(tc.to_f*pc.to_f/36000))-1)*360/(pl.to_f-pc.to_f)*100}"
  end
  
  #Futuro del dólar o Precio teórico del dólar
  def futur_doll(spot, tmx, pzo, tusa)
    "Futuro Dolar: #{spot.to_f*(1+(tmx.to_f*pzo.to_f/36000))/(1+(tusa.to_f*pzo.to_f/36000))}"
  end
  
  #Futuro del IPC y Acciones
  def fut_ipc_acc(pr, div, tasaxdias)
    "Futuro del IPC y Acciones: #{(pr.to_f-div.to_f)*(1+(tasaxdias.to_f/36000))}"
  end

  #Tasa Efectiva o Rendimiento Efectivo
  def tasa_efectiva(vf = 2.0, vi = 1.0)
    "Tasa Efectiva: #{((vf.to_f / vi.to_f) - 1)*100}"
  end

  #Rendimiento Anual
  def rto_anual(vf, vi = 1.0, dias)
    "Rendimiento Anual: #{((((vf.to_f / vi.to_f) - 1)*100)*360)/dias}"
  end

  #Interés Simple
  def int_simpl(monto, tasaxdias)
    "Interes Simple: #{(monto.to_f*tasaxdias.to_f)/36000}"
  end

  #Valor Futuro Continuo
  def vf_cont(vp, tnom, dias)
    "Valor Futuro Continuo: #{vp.to_f*(2.71828182846**(tnom.to_f*dias/36000))}"
  end

  #Tasa Real
  def t_real(tnom, infl)
    "Tasa Real: #{(((1+(tnom.to_f/100))/(1+(infl.to_f/100)))-1)*100}"
  end

  #Valor Instrinseco de Call
  def vi_call(spot, precio)
    spot.to_f - precio.to_f
  end

  #Valor Instrinseco de Put
  def vi_put(precio, spot)
    precio.to_f - spot.to_f
  end

  #Valor Extrinseco
  def v_extrinseco(prima, vi)
    prima.to_f - vi.to_f
  end
end
