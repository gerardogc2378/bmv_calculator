Gem::Specification.new do |s|
  s.name        = 'bmv_calculator'
  s.version     = '0.0.1'
  s.date        = '2013-07-16'
  s.summary     = "Calculadora BMV"
  s.description = "Calculadora básica para la BMV"
  s.authors     = ["Gerardo Gonzalez Cruz"]
  s.email       = 'gerardogc2378@gmail.com'
  s.files       = ["lib/bmv_calculator.rb"]
  s.homepage    = 'https://github.com/gerardogc2378/bmv_calculator.git'
  s.license     = 'MIT'
end
